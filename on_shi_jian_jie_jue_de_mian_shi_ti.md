# O(N) 时间解决的面试题

## 名人问题

> 有 n 个人他们之间是否认识用邻接矩阵表示(1表示认识，0表示不认识)，并且 A 认识 B 并不意味着 B 认识 A，名人定义为他不是认识任何人且所有人都认识他的人。求出所有名人。

分析：最多只能有 1 个名人

笨办法，遍历 i，检查每个 j，是否满足 i 不认识 j 且 j 认识 i。复杂度 O(n^2)。

O(n)的方法

+ 对于两个人 i 和 j
    + 如果 i 认识 j，那么 i 显然不是名人，删掉 i
    + 如果 i 不认识 j，则 j 显然不是名人，删掉 j
+ 最后剩余一个人，检查她是否是名人

**实现 1**

+ 用一个数组保存所有没检查人的编号
+ 数组如何删除 a[i]?
    + 不需要保证顺序的时候，只要 a[i] = a[--n] 即可，把最后一个元素移到 i 位置即可。

时间 O(n)，空间 O(n)

```
for (int i = 0; i < n; i++){
    a[i] = i;
}

while (n > 1){
    // 比较的话因为一直在把后面的移过来，所以只要比较前两个元素
    if (known[a[0]][a[1]]){
        a[0] = a[--n];
    }
    else {
        a[1] = a[--n];
    }
}

for (int i = 0; i < n; i++){
    if ((a[0] != i) && (known[a[0]][i] || !known[i][a[0]])){
        return -1;
    }
}
return a[0];
```

**实现 2**

一头扫，时间 O(n)，空间 O(1)

+ i < j
+ [0...i-1]没有名人
+ [i...j-1]没有名人
+ 如果 i 认识 j，删掉 i
    + i = j, j = j + 1
+ 如果 i 不认识 j，删掉 j
    + j = j + 1

```
int i = 0, j = 1
for (; j < n; j++){
    if (known[i][j]){
        i = j;
    }
}
for (j = 0; j < n; j++){
    if ((i != j) && (known[i][j] || !known[j][i])){
        return -1;
    }
}
return i;
```

**实现 3**

两头扫，时间 O(n)，空间 O(1)

+ i = 0, j = n - 1
+ i < j
    + [0...i-1] 没有名人
    + [j+1...n] 没有名人
    + 如果 i 认识 j，删掉 i，即 i++
    + 如果 i 不认识 j，删掉 j，即 j--

```
int i = 0, j = n - 1;
while (i < j){
    if (known[i][j]){
        i++;
    } else {
        j--;
    }
}
for (j = 0; j < n; j++){
    if ((i != j) && (known[i][j] || !known[j][i])){
        return -1;
    }
}
return i;
```

## 最大间隔问题

> 给定数组 a，求下标对 i, j 满足 a[i] <= a[j]，并且 j - i 最大

分析

+ 假设目前最优解是 d，对于 j，至少要检查 i = j - d - 1 才可能最优
+ 记录前缀最小值 p[x] = min{p[0..x]}
+ 倒着循环 j，对于每个 j 看一下 p[j-d-1] 是否 <= a[j]，用 p 引导，这里指的是通过 p 可以知道在这个 i 之前还有没有比 a[j] 更小的
+ 如果前面都比 a[j] 大，则这个 j 得不到更优的解

时间 O(n) 因为内层循环的值(best)始终在增大，一共循环了 n 次。

```
int run(vector<int> &a){
    int n = a.size();
    vector<int> p(n);
    for (int i = 0; i < n; i++){
        p[i] = ((i == 0) || (a[i] < p[i - 1])) ? a[i] : p[i - 1];
    }
    int best = 0;
    for (int j = n - 1; j > best; j--){
        while ((j > best) && (a[j] >= p[j - best - 1])){
            ++ best;
        }
    }
    return best;
}
```

## 01 相等的串

> 给定一个 01 串，求它一个最长的子串满足 0 和 1 的个数相等

分析

+ 把 0 看成 -1，1 当做 +1，使用前缀和
+ 需要两个前缀和相等，则这两个前缀和之间的子串满足 0 的个数和 1 的个数相等。
+ 对前缀和排序？O(nlogn)
+ 优化——不需要排序
    + 前缀和范围是[-n..n]，我们加上 n 之后就是[0..2n]，只要记录每个和第一次出现的位置
    + 本质是利用 hash 代替排序
    + 当 hash 值是比较小的非负整数时，可以用作数组下标

```
int run(char *s){
    int n = strlen(s);
    // 开一个长度为 2n + 1 的数组 (n << 1) | 1 ，并设为 -1
    vector<int> have((n << 1) | 1, -1);
    // 因为是从 n 开始，
    have[n] = 0;
    int sum = n;
    int best = 0;
    for (int i = 0; i < n; i++){
        sum += (s[i] == '0') ? (-1):(+1);
        if (have[sum] >= 0){
            best = max(best, i - have[sum] + 1);
        } else {
            have[sum] = i + 1;
        }
    }
}
```

方法比较 tricky，需要注意细节。

## 二进制矩阵中 1 的个数

> 给定 n*n 的 01 仿真，每一行都是降序的(即先连续的一段 1，再连续的一段 0)，求 1 最多的那行中 1 的个数

分析

+ 算法 1：输出每一行的 1。O(n^2)
+ 算法 2：二分除每一行 0 和 1 的分界线。O(nlogn)
+ 算法 3：如果某个位置是 1，则向右，是 0 则向下(只有找到比本行更多的 1 才有意义)

时间 O(n)

```
int run(vector<vector<char>> &a){
    int n = a.size();
    int best = 0;
    for (int i = 0; (best < n) && (i < n); i++){
        while ((best < n) && (a[i][best] == '1')){
            best++;
        }
    }
    return best;
}
```

## 下一个排列

Next Permutation, leetcode 31

关于字典序的理解：

a[0], a[1]...a[n-1] 的下一个排列是字典序比它大的最小的一个。

找到尽可能大的 m，b[0] = a[0], b[1] = a[1]....b[m-1] = a[m-1]，而 b[m] > a[m], b[m+1...n-1] 是按照升序(不减序)排列的.

+ 目前的排列是：`(A)a[x](B)`
+ 下一个排列是：`(A)a[y](B')`
    + A 是相同的，A 尽可能长
    + a[y] > a[x]
    + B' 几乎是 B 里面的数排好顺序的结果
+ 如何确定 x？
    + 一个位置只要右边有数比它大就是候选的 x
    + a[x] 是最后一个这样的数(最右边)
        + a[x] 右边的数，每个数的右边(后缀)没有比它大的
        + 所以 a[x] 右边的数是按照降序(不升序) 排列的
 

算法(二找、一交换、一翻转)

+ 找到最后一个严格升序的首位 (a[i] < a[i+1])，定义为 x
    + （A）= a[0...x-1] (B) = a[x+1...n-1]
+ 找到 y > x, a[y] > a[x],且 a[y] 最小(从右往前左找的第一个)
    + 一定存在，因为 x+1 就是一个候选
    + a[x] 后面的数都是降序，所以从后往前找到第一个大于 a[x] 的位置就是 y
+ 交换 a[x], a[y]
+ 对(x+1) 位后进行逆转
    + 交换后 a[x+1...n-1] 仍然是降序(不升)
    + 逆转等于排序

具体代码见 leetcode 部分

## 上一个排列

算法

+ 找到最后一个严格降序的首位 (a[i] > a[i+1])，定义为 x
    + （A）= a[0...x-1] (B) = a[x+1...n-1]
+ 找到 y > x, a[y] < a[x],且 a[y] 最小(从右往前左找的第一个)
    + 一定存在，因为 x+1 就是一个候选
    + a[x] 后面的数都是降序，所以从后往前找到第一个大于 a[x] 的位置就是 y
+ 交换 a[x], a[y]
+ 对(x+1) 位后进行逆转

## 均分 01

![](on1.jpg)

![](on2.jpg)

![](on3.jpg)

![](on4.jpg)

## X 的个数

![](on5.jpg)

![](on6.jpg)

## PAT 的个数

![](on7.jpg)

## 最小平均值子数组

![](on8.jpg)

结论：只考虑长度为 2 和 3 的段就可以了。因为『滑动窗口』，也可以直接接 US 那，因为 2 和 3 是常数

可以使用乘法代替除法避免精度问题

## 环形最大子数组和

![](on9.jpg)

## 允许交换一次的最大子数组和

![](on10.jpg)

![](on11.jpg)

# 问题分类介绍

## 1 最大子数组

![](on12.jpg)

![](on13.jpg)

## 2 循环移位

![](on14.jpg)

例 2.2 单词翻转 (字符串高频面试题 例5)

例 2.3 回文判断

## 3 快排 partition

![](on15.jpg)

![](on16.jpg)

## 4 众数问题

![](on17.jpg)

## 5 单调堆栈

![](on18.jpg)

## 6 单调队列

![](on19.jpg)

## 7 树相关

![](on20.jpg)

![](on21.jpg)

## 8 滑动窗口相关

![](on22.jpg)

![](on23.jpg)

![](on24.jpg)

## 9 链表

![](on25.jpg)

## 10 其他

![](on26.jpg)

![](on27.jpg)

![](on28.jpg)