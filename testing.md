## What the Interviewer Is Looking For

+ Big Picture Understanding
+ Knowing How the Pieces Fit Together
+ Organization
+ Practicality

## Testing a Real World Object

1. Who will use it? And why?
2. What are the use cases?
3. What are the bounds of use?
4. What are the stress / failure conditions?
5. How would you perform the testing?

## Testing a Piece of Software

1. Are we doing Black Box Testing or White Box Tesing?
2. Who will use it? And why?
3. What are the use cases?
4. What are the bounds of use?
5. What are the stress conditions / failure conditions
6. What are the test cases? How would you perform the testing?

## Testing a Function

1. Define the test cases
	+ The normal case
	+ The extremes
	+ Nulls and "illegal" input
	+ Strange input
2. Define the expected result
3. Write test code

## Troubleshooting Questions

1. Understand the Scenario
2. Break Down the Problem
3. Create Specific, Manageable Tests