# 排序查找实战

## 例1 杨氏矩阵查找

![](ss1.jpg)

![](ss2.jpg)

![](ss3.jpg)

## 例2 两个有序数组的中位数

![](ss4.jpg)

![](ss5.jpg)

## 例3 荷兰国旗问题

![](ss6.jpg)

## 总结

![](ss7.jpg)