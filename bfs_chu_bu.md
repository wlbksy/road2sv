# BFS 初步

## 队列操作

![](bfs1.jpg)

## 实现 1

![](bfs2.jpg)

## 实现 2

![](bfs3.jpg)

## BFS 算法简介

![](bfs4.jpg)

## BFS 算法框架

![](bfs5.jpg)

## 例1 八数码问题

![](bfs6.jpg)

## 例2 word ladder

![](bfs7.jpg)

![](bfs8.jpg)