# Debugger

Explain what the following code does: ((n & (n-1)) == 0)

## Solution

n = abcde1000
n - 1 = abcde0111

can only have one 1 in the binary form -> check power of 2