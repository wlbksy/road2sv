# 概率面试题精讲

## 独立的理解

![](pb1.jpg)

## 构造随机数发生器

![](pb2.jpg)

![](pb3.jpg)

![](pb4.jpg)

## 不均匀随机数发生器构造均匀

![](pb5.jpg)

## 随机变量的和

![](pb6.jpg)

## 水库(Reservoir)采样

![](pb7.jpg)

![](pb8.jpg)

![](pb9.jpg)

## 随机排列产生 random_shuffle

![](pb10.jpg)

## 带权采样问题

![](pb11.jpg)

![](pb12.jpg)

![](pb13.jpg)

![](pb14.jpg)

## 总结

![](pb15.jpg)