# 简历内容与格式

1. 保证在一页以内
2. 包含和申请职位最相关的内容
3. 以最相关的顺序排列内容
4. 使用 action verbs 和 highlight achievements
5. 避免使用机构，职位的缩写，例如不要写 CMU，要写 Carnegie Mellon University
6. 包含校园地址和永久地址
7. 包含你的 email 地址

**格式**

1. 简约，留白
2. 浅色纸，标准 8 1/2" x 11"
3. 如果有多页，单页打印
4. 用可读性强的10-14号字体
5. 可以利用不同的格式来凸现差异，一个短语不要用两种格式(例如粗体斜体)

# 各部分详细说明

**Identifying Information** 名字，现在地址和永久地址，电话号码，电子邮件，个人网站(可选)，也可以在这个部分标明是否是美国公民

**Education** 学校名称和绩点(如果绩点在3.0以上)，也包括学位名称等信息

**Projects** 课程项目可以列在这里，高亮相关技术以及团队合作技能，用短语描述项目，每一行用一个 action verb 开头。在项目中的角色，项目的目的以及如何解决难题

**Skills** 职业相关技能

**Experience** 在相关工作中的经验

**Honors** 获奖情况，奖学金等

**Achievements** 表演，展出，论文发表

# 网申注意事项

1. 用 sans serif 字体(Arial, Helvetica)
2. 电话号码另起一行
3. 注意用**名词**和**动词**来描述技能、经验以及学历。
4. 使用常见的标题，例如：Objective, Education, Experience, Employment, Positions Held, Accomplishments, Skills, Summary of Qualifications, Strengths, Affiliations, Publications, Papers, Licenses, Certifications, Examinations, Honors, and References.
5. 用本行业的专业术语
6. 避免粗体斜体下划线，如果字母没有挤到一起，也可以用大写字母
7. 表格中一栏不要另起一行，可能会看不到

# Cover Letter

大致内容可以一致，但是需要针对不同的公司进行个性化定制

## 内容及具体说明

**第一段 - 介绍 (2-4 句话)**

告知写信的目的，让面试官有兴趣读下去

+ 提及你的推荐人(如果有的话)
+ 说明为何要写这封信，具体点出工作职位与类型
+ 告知从哪里听说这个消息和为什么感兴趣
+ 简要介绍学历，专业，学校以及毕业时间

**第二/三段 - 主体 (1-2 段，取决于背景)**

通过展示你对公司所做的调研以及你的技能有多匹配来让面试官产生兴趣

+ 标明自己可以如何帮助公司，多写你能做什么而不是你多想要这个职务
+ 高亮与工作相关的成就，技能和经验
+ 亮兵器，证明你应该被邀请来参加面试
+ 不要只是简单重复简历中的内容而是点出重要的经验和关键技能，展示简历中展现不出来的内容，比如说个人品质等等

**第四段 - 结束 (至多 4 句话)**

许下承诺

+ 倡议下一步的计划，是约面试还是要求其他更多的信息
+ 告知对方你的 availability
+ 提及简历或相关工作
+ 重述联系方式
+ 感谢面试官

# Action Verbs Table

下面是用于不同场合的 action verbs 列表，请对照简历中不同部分的描述来进行修正，力求简明扼要且突出中心思想。

### Management/Leadership Skills

+ administrator, analyze, apply, approve, assign, attain
+ chair, conduct, consolidate, contract, coordinate
+ delegate, determine, develop, direct
+ enlist, evaluate, execute, formalize, form, found
+ hire, implement, improve, increase, initiate, inspire
+ led, manage, operate, organize, oversee
+ pioneer, plan, prioritize, produce, promote
+ recommend, recruit, represent, review
+ schedule, select,solve, spearhead, sponsor, start
+ strengthen, supervise, train, utilize

### Communication Skills

+ address, arbitrate, arrange, author, brief
+ collaborate, consult, contact, convince, correspond
+ demonstrate, develop, direct, dissuade, draft
+ edit, enlist, explain, familiarize, formulate
+ influence, inform, interpret, listen
+ market, mediate, moderate, motivate, negotiate
+ persuade, present, promote, publish, publicize
+ reconcile, recruit, report, respond
+ secure, sell, solicit, speak, summarize, translate, write

### Research/Technical Skills

+ analyze, assemble, assess, build, calculate
+ clarify, collect, compile, compute, construct, critique
+ design, devise, diagnose, disassemble, discover, document
+ engineer, evaluate, examine, extract, fabricate, gather
+ identify, inspect, install, interpret, interview, investigate
+ maintain, model, operate, organize, overhaul, program, remodel
+ repair, research, review, salvage, search, solve, summarize
+ survey, systematize, test, troubleshoot, uncover, upgrade

### Teaching Skills

+ adapt, advise, clarify, coach, communicate, coordinate
+ deliver, demystify, develop, educate, enable, encourage
+ evaluate, explain, facilitate, guide, inform, instruct, introduce
+ lecture, persuade, set, goals, stimulate, teach, train, tutor

### Financial Skills

+ administer, allocate, analyze, appraise, audit
+ balance, budget, calculate, compute, decrease, develop
+ finance, forecast, manage, market, plan, project
+ reconcile, reduce, research

### Creative Skills

+ act, author, compose, conceptualize, concern, create, customize
+ design, develop, devise, direct, establish, fashion, find
+ illustrate, initiate, institute, integrate, introduce, invent
+ originate, perform, plan, revamp, revitalize, revolutionize, shape

### Customer Service / Teamwork Skills

+ aid, assess, assist, attend, clarify, coach, collaborate
+ compromise, confront, contribute, cooperate, counsel
+ demonstrate, diagnose, elaborate, empathize, encourage, expedite
+ facilitate, familiarize, guide, help, instill, mentor, motivate
+ provide, reflect, refer, represent, resolve
+ serve, settle, support, treat

### Administrative / Organizational / Clerical Skills

+ approve, arrange, assemble, catalogue, centralize, classify
+ collect, compile, coordinate, dispatch, distribute
+ enforce, execute, handle, implement, inspect, maintain, monitor
+ operate, organize, plan, prepare, process, purchase
+ record, reorganize, retrieve, schedule, screen, specify
+ systematize, tabulate, update, validate

### More Verbs For Accomplishments

+ achieved, boosted, broadened, completed, eliminated, ensured
+ exceeded, excelled, expanded, expedited, gained, generated
+ improved, launched, mastered, modernized, pioneered
+ reduced(losses), resolved(problems), restored
+ saved, supplemented, transformed