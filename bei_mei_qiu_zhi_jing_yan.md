# 北美求职经验

流程

+ LinkedIn
+ 网申
+ 找内推
+ 通过比赛 - hackerrank, codeforce, topcoder
+ 被 HR 联系
+ 面试(电面, Onsite)
+ Offer / 被拒
+ 签证
+ 入职

公司列表

+ Facebook
+ LinkedIn
+ Amazon
+ Google
+ Microsoft
+ Twitter
+ Storm8
+ Apple
+ Pocket Gems
+ Tango
+ A Thinking Ape

H1B

+ 工作签证、非移民签证
+ 一定要有 offer 才能办
+ 有效期 3 年，可以再延长 3 年
    + 申请移民签证(绿卡)到某个阶段，可以保证身份有效
+ 流程
    + 每年 4 月 1 号所有申请提交移民局。硕博先抽，没抽中还可以再抽一次
    + 拿到名额，准备面签
    + 10 月 1 日签证生效，9 月底可入境

面试准备

+ 非技术
    + 介绍自己、项目、如何协同、对现有产品改进
+ Online Assignment(OA)
+ 电面
+ 逻辑题
    + 概率
    + 数学
+ 开放问题
+ 系统设计
+ 测试