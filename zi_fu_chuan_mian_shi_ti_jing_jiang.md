# 字符串面试题精讲

## 字符串简介

![](str1.jpg)

## 面试题总体分析

![](str2.jpg)

## 例1 01交换

![](str3.jpg)

## 例2 字符的替换和复制

![](str4.jpg)

![](str5.jpg)

## 例3 交换 * 号

![](str6.jpg)

![](str7.jpg)

![](str8.jpg)

## 例4 子串变位词

![](str9.jpg)

![](str10.jpg)

![](str11.jpg)

![](str12.jpg)

## 例5 单词翻转

![](str13.jpg)

## 总结

![](str14.jpg)