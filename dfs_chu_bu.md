# DFS 初步

## 堆栈操作

![](dfs1.jpg)

## 实现

![](dfs2.jpg)

## DFS 算法简介

![](dfs3.jpg)

## 例1 二叉树的三种遍历

前序中序后序，注意细节实现即可

## 例2 n 皇后问题

在一个国际象棋棋盘上放置 n 个皇后，使得它们不能互相攻击。即没有两个皇后在同一行、列、对角线

### dfs 递归框架

![](dfs4.jpg)

## 比较 DFS 与 BFS

![](dfs5.jpg)

![](dfs6.jpg)

## 扩展应用

![](dfs7.jpg)

![](dfs8.jpg)

