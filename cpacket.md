# cPacket

## 公司介绍

主要是给大公司的 data center 提供进一步的底层封装和技术支持，比如说 google 或者是 facebook 的 data center，他们不用直接去操心 data center，而可以直接使用 cPacket 提供的 web 界面来实时了解和监控运行状况。并且 cPacket 也会负责注入安全，部署，攻击防范等。

公司目前工程师大概 40 个，分成 4 个 team

1. Hardware/Firmware: 主要是负责硬件以及相关的底层开发 —— FPGA
2. Software: 主要是 web 界面开发 —— Python
3. Solution: 主要是在客户的 data center 那边来处理各种事务
4. QA

今天面试我的是 firmware 的 engineer。

提供午餐、零食、周五有 happy hour，上班8-11点，下班5-8点，在 Mountain View。服务运行在自己的服务器上。

接下来如果顺利是两轮电面之后 onsite。看看自己能走多远了

## Phone Screen

Mark Lewis，也是 firmware group 的小哥，问了我三个题目

1. 如何把十进制转换为二进制，分别用递归，循环和位操作来实现
2. 返回链表的倒数第 n 个节点
3. 如何设计一个文件系统

