# Dominos

There is an 8x8 chessboard in which two diagonally opposite corners have been cut off. You are given 31 dominos, and a single domino can cover exactly two squares. Can you use the 31 dominos to cover the entire borad? Prove it..

## Solution

Black and white squares, 32 black vs 30 white while 1 domino can only cover 1 black and 1 white -> impossible to cover the board.S