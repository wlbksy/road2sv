# 30秒自我介绍

关键词: Da Wang, Graduate Student, Electrical and Computer Engineering, Carnegie Mellon University, Undergraduate major, Software Engineering, software and hardware, computer graphic, machine learning and game development,position, resume


Hi, my name is Da Wang. I'm a gradutate student majored in Electrical and Computer Engineering. Finishing my undergraduate in Software Engineering, I have a deep understanding about software as well as hardware. Also I had great experience on computer graphics, machine learning as well as mobile phone development. Here is my resume and I'd like to know more about [company name] and chances for working in [company name].

# 1分钟项目介绍

+ Situation(why you do this project)
+ Action(language, technique, your role)
+ Output(what you get)

## Information Management with Knowledge Graph

+ So much information everyday, facebook/twitter/news paper/etc
+ Connection is the key for converting information to knowledge
+ How can we know they are connected?
+ Usually: Text similarity, Topic Model -> formula solution
+ My solution: Knowledge graph
+ Built from Wiki
+ note taking application & book recommender system
+ Python, Web UI, and ML/NLP

## L0 Smoothing for iOS device

+ Inspired by a paper in SIGGRAPH Aisa 2012
+ First L0 smoothing algorithm implemented on mobile devices
+ Suppress noise and maintain edges
+ comic-style photographs
+ Basis for further image processing

## Basic Speech Recognition System

+ Course Project, C++
+ HMM model for recognizing continuous speech

## Wiki for League of Legneds

+ WP7/8 platform
+ Assistant application for players to learn the latest information about different heros and the game mechenism
+ Over 700K downloads with an average 4.5 stars
+ Featured application recommended by Nokia in Windows phone app store