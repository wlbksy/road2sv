# 贪心算法初探

## 贪心算法简介

![](ga1.jpg)

## 贪心算法的框架

![](ga2.jpg)

## 贪心算法与动态规划

![](ga3.jpg)

## 例1 换零钱

![](ga4.jpg)

## 例2 活动安排问题

![](ga5.jpg)

![](ga6.jpg)

## 例3 01背包问题

![](ga7.jpg)

## 总结

![](ga8.jpg)

![](ga9.jpg)