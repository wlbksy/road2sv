# 行动日志

投简历要多多益善，周日晚上花上两个小时时间把截至日期到下周日的所有 jop opening 都投一遍。重点使用学校提供的 career service 网站。

日期 | 行动 | 备注
:---: | :---: | :---:
Aug 24 | 注册 TartanTRAK | 等待学校发来密码以便登录
Aug 24 | 更新 LinkedIn | 修改完简历后每天投一些
Aug 24 | 登录 TartanTRAK 注册 workshop | 已经添加到日历中
Aug 24 | 预约 appointment 改简历，准备 job fair 和 了解业界 | 事先务必做好充足准备
Aug 25 | 简历初稿完成 | 依据 ECE 模板
Aug 25 | 简历用词修改指南完成 | 更新简历初稿
Aug 25 | Cover Letter 初稿完成 | 依据模板及指南
Aug 25 | Cover Letter 用词修改指南完成 | 更新 Cover Letter
Aug 25 | Android Engine Engineer @ PennyPop | 试水
Aug 25 | Software Engineer @ Visual Concepts(2K) | cmu email, w33
Aug 25 | 更新 LinkedIn 简历 | 去掉无用和更新有用
Aug 26 | 找 CPDC 修改简历 | 简历更新至 0826版本
Aug 26 | 收到 Visual Concepts 的回复 | 要 code sample
Aug 26 | 投了截止到 9.1 日的合适的 4 间公司 | 下一次投 9.1 - 9.7 截止的
Aug 27 | 整理好 Code Sample (C++) | 已回复给 Visual Concepts
Aug 28 | 复习机器学习相关概念 | 纸质版笔记
Aug 30 | Visual Concepts 进入电话联系环节 | 了解 2K 公司，准备电面
Sep 1 | 简历已投至 9.7 日合适的 11 间公司 | 下一次投 9.8 - 9.14 截止的
Sep 2 | 和 VC 的 Scott Patterson 聊了一下 | 做 code Challenge
Sep 2 | BrightEdge 预约电话面试 | 9.11 上午 10 点
Sep 2 | LiveRamp 发来 Code Challenge | 三天内做完
Sep 4 | 提交 VC 的代码，做完 LR 的 OA | 等待结果
Sep 7 | 简历已投至 9.13 日合适的 14 间公司 | 下一次 9.15 - 9.21 的
Sep 14 | 简历已投至 9.21 日合适的 19 间公司 | 下一次 9.22 - 9.27 的
Sep 21 | 简历已投至 9.28 日合适的 22 间公司 | 下一次 9.29 - 10.6 的
Sep 27 | 简历已投至 10.12 日合适的 26 间公司 | 下一次 10.13- 10.19 的
Sep 28 | TOC 第一天十来家公司 | 明天继续投


# 已投公司列表

1. Android Engine Engineer @ PennyPop
2. (**Code Assignment -> Rejected**)Software Engineer @ Visual Concepts(2K) cmu, w33 # 100K
3. Software Engineer - Ad Network/Machine Learning @ Apple
4. (**Rejected**)Data Infrastructure Software Engineer @ Voleon Capital Management LP
5. Data Scientist @ EngageClick Inc
6. Software Developer @ 2redbeans
7. Graphics Architecture Engineer @ NVIDIA
8. OpenGL Software Engineer(MAC) @ NVIDIA
9. (**Phone Screen**)Software Engineer @ BrightEdge # 100K
10. (**Rejected**)Generalist Software Engineer @ LiveRamp # 130K
11. (**Rejected**)Software Engineer @ Course Hero
12. (**Phone Screen**)Software Development Engineer @ Yahoo
13. (**Phone Screen**)Software Engineer @ cPacket Networks Inc
14. (**Rejected**)Mobile Engineer @ Expensify
15. (**Rejected**)Software Engineer @ Aribnb
16. (**2nd Phone Screen -> Rejected**)Server Software Engineer @ zazzle
17. Systems Engineer @ Sojern
18. Software Developer @ Epic
19. (**Rejected**)Software Engineer @ Eventbrite (要做题，有点意思)
20. (**Rejected**)Software Engineer, New College Grad @ Medallia
21. Software Engineer - Computer Vision @ Samsung
22. Software Engineer - Android and Embedded Systems/IOT, Wearables, Home Automation @ Samsung
23. (**Rejected**)Software Engineer - New Grad @ Thumbtack
24. (**Code Challenge**)VMware Full Time @ VMware
25. Software Development Engineer, New Graduate @ Groupon
26. Engineering positions for US location @ DJI
27. (**Rejected**)Software Engineer @ Snapchat, LA
28. (**Rejected**)Software Engineer @ twilio
29. (**Rejected**)Software Engineer @ Intentional
30. Data Platform Software Engineer @ AppDynamics
31. (**On Campus -> Rejected**)Software Engineer @ Xcalar
32. Software Engineer @ Navis LLC
33. Mobile Engineer @ Wish
34. Software Engineer @ WorldQuant, LLC
35. Research Engineer @ Trooly Inc
36. Software Engineer @ Adobe
37. Backend Software Developer @ Pixured, Inc
38. Software Engineer in Test (San Francisco) @ Leanplum
39. iOS Software Engineer (San Francisco) @ Leanplum
39. Software Developer @ Oracle Corporation
40. Software Engineer @ Nextdoor.com, Inc
41. Software Engineer @ PatternEx
42. (**Rejected**)Software Engineer @ Domeyard LP
43. Software Engineer @ Hulu
44. Data Engineer @ Qadium
45. iOS Developer @ Trance
46. Software Engineer, Distributed Systems @ Turn
47. Web and Mobile Software Engineers @ Quizlet
48. Entry Level Mobile Implementation Engineer @ Ensighten Inc.
49. (**Rejected**)Software Engineer - New Grad 2016 @ Dataminr
50. Software Engineer/Mobile Application Developer @ Laserfiche
51. (**HR Interview**)Software Engineer @ Caliper Corporation
52. Software Engineers @ bebop
53. Junior Software Engineer @ Data Capital Management
54. (**Code Challenge**)Software Engineer - New Grad @ Quora
55. 2016 New Graduate - Software Engineering @ Quantcast
56. Software Engineer @ imo.im
57. ios @ wealthfront
58. Software Developer @ Tower Research Capital LLC
59. C++ Software Engineer @ KCG Holdings
60. Software Engineer @ Noom, Inc.
61. Software Engineer @ Amplify Education, Inc
62. Software Engineer - New Graduate @ Whitepages.
63. Application Developer @ ClickTime
64. Software Engineer – NLP & Computational Linguist @ Seerflix Inc
