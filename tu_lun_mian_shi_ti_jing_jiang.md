# 图论面试题精讲

## 简介

![](gr1.jpg)

## 面试题总体分析

![](gr2.jpg)

## 例1 遍历序列

![](gr3.jpg)

## 例2 二叉树相关问题 递归

![](gr4.jpg)

![](gr5.jpg)

![](gr6.jpg)

![](gr7.jpg)

![](gr8.jpg)

![](gr9.jpg)

## 例3 二叉树与链表

![](gr10.jpg)

![](gr11.jpg)

![](gr12.jpg)

## 例4 无向图复制

![](gr13.jpg)

## 例5 直角遍历棋盘

![](gr14.jpg)

![](gr15.jpg)
