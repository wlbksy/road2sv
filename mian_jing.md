这里是每个我遇到的公司的面经，也包含其他人遇到的问题，算是我准备的过程

重复的题目会在题目前用 @ 标出，这样方便检索高频题目

## 高频

1. Merge/Insert intervals I and II
2. Word break I and II
3. Valid parenthese （what if the input string is a stram and it’s super long?)
4. Binary Tree Iterator (pre + in)
5. Zigzag Iterator (N lists, cannot directly access by index)
6. Perfect square