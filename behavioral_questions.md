# Interview Preparation Grid

针对每个 project，列出以下几个方面的相关总结关键词，一定要是**特别精炼的短语**，不要长篇大论的句子：

+ Challenges
+ Mistakes/Failures
+ Enjoyed
+ Leadership
+ Conflicts
+ What You'd Do Differently

## 细致准备重点项目

两三个 project 要重点准备，除了上面提到的方面，还需要能够细致介绍：

+ Technical decisions
+ Choices of technologies(tradeoff)

# 常见面试问题

> Tell me about yourself

可以按照如下的方式组织准备

1. **Current Role [Headline Only]**: I'm a graduate student in Carnegie Mellon University majored in Electrical and Computer Engineering.
2. **College**: I majored in Software Engineering for my undergraduate and had a 6-month internship in Microsoft.
3. **Current Role [Details]**: Learning both the software and the hardware, I know how to write great code on different platforms, especially computer vision and machine learning.
4. **Outside of Work**: Outside of work, I've been working on mobile app development. One of my app named League of Legends Wiki has over 700K downloads with an average rating of 4.5 out of 5.
5. **Wrap Up**: I'm looking for a full time job as I'll get my master degree next year and your company caught my eye. I've always been interested in creating something new so I'd like to talk more with you about the xxx position in your company.

注意见缝插针描述自己的闪光点，让人感兴趣和印象深刻。

> What are your weaknesses

回答技巧：实话实话，不要装逼。点明缺点之后重点强调自己是如何克服的。

个人参考答案：Sometimes, I may lose focus on the whole project while plunge into very detailed problems. It's not bad to spend more time finding the best solution. But it may be better to finish the most critical part first. As it is, I'll draw the whole design on paper and put it just in front of the monitor so that I can easily find out what I should focus on.

# 问面试官的问题

大概有三类问题

## Genuine Questions

跟公司，工作有关的问题，例如

1. What brought you to this company?
2. What has been most challenging for you?
3. Do you have program managers? If there is a conflicts between developer and managers, how do you solve it?

## Insightful Questions

这类问题通常需要对公司有比较深入的研究，例如

1. I noticed that you use technology X. How do you handle problem Y?
2. Why did this product choose to use technology X over technology Y?

## Passion Questions

展现激情和学习兴趣

1. I'm very interested in machine learning, and I'd love to learn more about it. What opportunities are there at this company to learn about this?
2. I'm not familiar with technology X, but it sounds like a very interesting solution. Could you tell me a bit more about how it works?

# 回答问题技巧

+ 不要过多涉及细节，而是用数据对比或者面试官能听懂的内容来介绍
+ 多说 I 而不是 We，说自己扮演的角色和所做的工作
+ 结构式问题回答

## Nugget First

开门见山，先用一句话概括，然后再逐步推进到细节部分

## S.A.R. (Situation, Action, Result)

+ outline the situation
+ explain the actions you took
+ describe the result

**Action 部分是最需要着力的地方，逻辑清晰，分点叙述，主要不要涉及过多细节**。

精雕细琢故事部分，字里行间体现出自己的一些特质，如：Initiative/Leadership, Empathy, Compassion, Humility, Teamwork/Helpfulness