# 最大连续子数组

![](lca1.jpg)

## 暴力法

![](lca2.jpg)

## 分治法

![](lca3.jpg)

![](lca4.jpg)

![](lca5.jpg)

## 分析法

![](lca6.jpg)

## 动态规划

![](lca7.jpg)