# 表现技巧

+ Whiteboards ten to encourage candidates to speak more and explain their thought process.
+ Getting a hard question isn't a bad thing.
+ If you haven't heard back from a company within 3-5 business days after your interview(1 week after on-site), check in (politely) with your recruiter.

# 基本面试流程

+ (1-2)phone-screen interview
+ (3-6)on-site