# 带重复元素的排列

给出一个具有重复数字的列表，找出列表所有不同的排列

样例

    给出列表[1,2,2]，不同的排列有：
    [
        [1,2,2],
        [2,1,2],
        [2,2,1]
    ]

挑战

    能否不使用递归完成？

## 题解

用 hashset 就不用考虑重复的问题

```java
class Solution {
    /**
     * @param nums: A list of integers.
     * @return: A list of unique permutations.
     */
    public ArrayList<ArrayList<Integer>> permuteUnique(ArrayList<Integer> nums) {
        Set<ArrayList<Integer>> result = new HashSet<ArrayList<Integer>>();
        Collections.sort(nums);
        int[] visited = new int[nums.size()];

        result.add(new ArrayList<Integer>());
        for (int i = 0; i < nums.size(); i++) {
            Set<ArrayList<Integer>> nextResult = new HashSet<ArrayList<Integer>>();
            for (ArrayList<Integer> l : result) {
                for (int j = 0; j <= l.size(); j++) {
                    //skip duplicates
                    //while (j < l.size() && nums.get(i) == nums.get(j)) {
                    //    j++;
                    //}
                    l.add(j, nums.get(i));
                    nextResult.add(new ArrayList<Integer>(l));
                    l.remove(j);
                }
            }
            result = nextResult;
        }
        return new ArrayList<ArrayList<Integer>>(result);
    }
}
```

递归的办法

```java
class Solution {
    /**
     * @param nums: A list of integers.
     * @return: A list of unique permutations.
     */
    public ArrayList<ArrayList<Integer>> permuteUnique(ArrayList<Integer> nums) {
        // write your code here
        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
        if (nums == null || nums.size() == 0) {
            return result;   
        }
        Collections.sort(nums);
        int[] visited = new int[nums.size()];
        helper(nums, result, visited, new ArrayList<Integer>());
        return result;
    }
    
    
    private void helper(ArrayList<Integer> nums, ArrayList<ArrayList<Integer>> result, int[] visited, ArrayList<Integer> cur) {
        if (cur.size() == nums.size()) {
            result.add(new ArrayList<Integer>(cur));
            return;
        }
        for (int j = 0; j < nums.size(); j++) {
            //if this number has been used or if the previous duplicate number has finished this position (so its visited flag is set to 0)
            if(visited[j] == 1 || (j != 0 && nums.get(j) == nums.get(j - 1) && visited[j - 1] == 0)){ 
                continue;
            }
            visited[j] = 1;
            cur.add(nums.get(j));
            helper(nums, result, visited, cur);
            visited[j] = 0;
            cur.remove(cur.size() - 1);
        }
    }
}
```